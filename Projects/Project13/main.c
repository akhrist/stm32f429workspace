#include "stm32f429xx.h"
#include "main.h"
#include "stdio.h"
#include "math.h"
#include "string.h"
#include "stdlib.h"
#include "stm32f4xx_hal.h"
#include "stm32f429i_discovery_lcd.h"

char* weekdays[8] = {"Day", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
uint8_t city[20];
char data[1000];
int Button_IT, RTC_IT, UART_IT;

UART_HandleTypeDef huart5;
RTC_HandleTypeDef hrtc;
RTC_TimeTypeDef sTime;
RTC_DateTypeDef sDate;
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_UART5_Init(void);
static void MX_RTC_Init(void);
void GetTime(void);

void Data_to_Time(int delay)
{  
  int hours, minutes, seconds; int year, month, day, weekday;
  char* ptr;
  ptr = strstr((char*)data, "datetime:");
  
  char temp[2];
  strncpy(temp, ptr+21, 2); hours = atoi(temp);
  strncpy(temp, ptr+24, 2); minutes = atoi(temp);
  strncpy(temp, ptr+27, 2); seconds = atoi(temp);
  
  strncpy(temp, ptr+12, 2); year = atoi(temp);
  strncpy(temp, ptr+15, 2); month = atoi(temp);
  strncpy(temp, ptr+18, 2); day = atoi(temp);

  ptr = strstr((char*)data, "day_of_week");
  weekday = atoi(ptr + 13);
  
  seconds += delay;
  if(seconds >= 60) { seconds = 0; minutes += 1;}
  if(minutes >= 60) { minutes = 0; hours += 1;}
  if (hours >= 24) { hours = 0; day += 1; weekday += 1; if(weekday > 7) weekday = 1;}
  weekday = (weekday == 0)? 7 : weekday;
  
  sTime.Seconds = seconds; 
  sTime.Minutes = minutes;
  sTime.Hours = hours;
  sDate.Date = day;
  sDate.Month = month;
  sDate.WeekDay = weekday;
  sDate.Year = year;
  HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
  HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
}

void DisplayTime(void)
{
  int hours, minutes, seconds; int year, month, day, weekday;
  HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
  HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
  seconds = sTime.Seconds;
  minutes = sTime.Minutes;
  hours = sTime.Hours;
  day = sDate.Date;
  month = sDate.Month;
  year = sDate.Year;
  weekday = sDate.WeekDay;
  
  BSP_LCD_SetFont(&Font24);
  
  uint8_t str_time[5];
  char* str1; char* str2; char* str3;
  str1 =(hours > 9)? "" : "0";
  str2 =(minutes > 9)? "" : "0";
  str3 =(seconds > 9)? "" : "0";
  sprintf((char*)str_time, "%s%d:%s%d:%s%d", str1, hours, str2, minutes, str3, seconds);
  BSP_LCD_DisplayStringAt(0, 230, str_time, CENTER_MODE);
  
  // The clock
  int r = 92; float pi = 3.14159;
  int x, y;
  BSP_LCD_SetTextColor(LCD_COLOR_BLACK); BSP_LCD_FillCircle(120,110,r-1);
  BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
  BSP_LCD_DrawCircle(120, 110, r);
  float phi1 = seconds*2*pi/60;
  float phi2 = minutes*2*pi/60;
  float phi3 = hours*2*pi/60;
  
  x = (int)(r*sin(phi1));
  y = (int)(r*cos(phi1));
  BSP_LCD_DrawLine(120, 110, 120 + x, 110 - y);
  x = (int)((r*3/4)*sin(phi2));
  y = (int)((r*3/4)*cos(phi2));
  BSP_LCD_DrawLine(120, 110, 120 + x, 110 - y);
  x = (int)((r/2)*sin(phi3));
  y = (int)((r/2)*cos(phi3));
  BSP_LCD_DrawLine(120, 110, 120 + x, 110 - y);

  // Display date:
  BSP_LCD_SetFont(&Font20);
  uint8_t str_day[10];
  str1 = (day > 9)? "" : "0";
  str2 = (month > 9)? "" : "0";
  str3 = (year > 9)? "" : "0";
  sprintf((char*)str_day, "%s%d %s%d 20%s%d", str1, day, str2, month, str3, year);
  BSP_LCD_DisplayStringAt(20, 265, str_day, LEFT_MODE);
  sprintf((char*)str_day, "%s", weekdays[weekday]);
  BSP_LCD_DisplayStringAt(180, 265, str_day, LEFT_MODE);
  BSP_LCD_DisplayStringAt(0, 292, city, CENTER_MODE);
}

int main()
{
  sprintf((char*)city, "City");
  Button_IT = 0; RTC_IT = 0; UART_IT = 0;
  
  HAL_Init();
  SystemClock_Config();
  MX_GPIO_Init();
  MX_UART5_Init();
  MX_RTC_Init();
  BSP_LCD_Init();
  
  // LCD
  BSP_LCD_LayerDefaultInit(LCD_BACKGROUND_LAYER, LCD_FRAME_BUFFER);
  BSP_LCD_SelectLayer(LCD_BACKGROUND_LAYER);
  BSP_LCD_DisplayOn();
  BSP_LCD_Clear(LCD_COLOR_BLACK);
  BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
  BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
  
  DisplayTime();
  
  while(1)
  {
    if(Button_IT == 1){
      GetTime();
      BSP_LCD_Clear(LCD_COLOR_BLACK);
      DisplayTime();
      Button_IT = 0;}
    else if (RTC_IT == 1){
      RTC_IT = 0;
      DisplayTime();}
  }
}

void GetTime()
{
  BSP_LCD_SetFont(&Font24);
  BSP_LCD_DisplayStringAt(0, 150, "Please wait", CENTER_MODE);
  
  char temp[20];
  sprintf(temp, (char*)city);
  memset(city, 0, 20);
  sprintf((char*)city, "City");
  HAL_UART_Transmit(&huart5, "Time request\r", strlen("Time request\r"), 20);
  HAL_Delay(100);
  HAL_UART_Receive(&huart5, city, 20, 100);
  if (strstr((char*)city, "City") != NULL){
    BSP_LCD_DisplayStringAt(0, 150, "Disconnected", CENTER_MODE);
    sprintf((char*)city, temp);
    memset(temp, 0, 20);
    HAL_Delay(1000);
    return;}
   
  uint8_t raw_data[1000];
  HAL_UART_Receive_IT(&huart5, raw_data, 1);
  while(UART_IT == 0);
  HAL_UART_Receive(&huart5, raw_data, 1000, 1000);
  memset(data, 0, 1000);
  strcpy(data, (char*)raw_data);
  memset((char*)raw_data, 0 , 1000);
  UART_IT = 0;
  
  if (strstr(data, "Server error") != NULL){
    BSP_LCD_DisplayStringAt(0, 150, "Server error", CENTER_MODE);
    HAL_Delay(1000);
    return;}
  if(strstr(data,"datetime") == NULL){
    BSP_LCD_DisplayStringAt(0, 150, "Unknown error", CENTER_MODE);
    HAL_Delay(1000);
    return;}
  
  Data_to_Time(1);
}

static void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};
  
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 180;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  HAL_PWREx_EnableOverDrive();

  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK |
  RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK; 
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5);
  
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct);
}

static void MX_UART5_Init(void)
{
  /**UART5 GPIO Configuration
    PC12     ------> UART5_TX
    PD2     ------> UART5_RX
    */
  __HAL_RCC_UART5_CLK_ENABLE();
  
  huart5.Instance = UART5;
  huart5.Init.BaudRate = 115200;
  huart5.Init.WordLength = UART_WORDLENGTH_8B;
  huart5.Init.StopBits = UART_STOPBITS_1;
  huart5.Init.Parity = UART_PARITY_NONE;
  huart5.Init.Mode = UART_MODE_TX_RX;
  huart5.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart5.Init.OverSampling = UART_OVERSAMPLING_16;
  HAL_UART_Init(&huart5);
  
  HAL_NVIC_EnableIRQ(UART5_IRQn);
}

static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  
  /*Configure GPIO pin: PD2, PC12 */
  GPIO_InitStruct.Pin = GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF8_UART5;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF8_UART5;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PA0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
}

void MX_RTC_Init()
{
  RTC_AlarmTypeDef sAlarm = {0};
  __HAL_RCC_RTC_ENABLE();

  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  HAL_RTC_Init(&hrtc);
      
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
      
  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x1;
  HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
  
  sAlarm.AlarmTime.Hours = 0;
  sAlarm.AlarmTime.Minutes = 0;
  sAlarm.AlarmTime.Seconds = 0;
  sAlarm.AlarmTime.SubSeconds = 0x0;
  sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
  sAlarm.AlarmMask = RTC_ALARMMASK_ALL;
  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
  sAlarm.AlarmDateWeekDay = 0x0;
  sAlarm.Alarm = RTC_ALARM_A;
  HAL_RTC_SetAlarm_IT(&hrtc, &sAlarm, RTC_FORMAT_BIN);
   
  HAL_NVIC_EnableIRQ(RTC_Alarm_IRQn);
}

void EXTI0_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
  Button_IT = 1;
}

void UART5_IRQHandler(void)
{
  HAL_UART_IRQHandler(&huart5);
  UART_IT = 1;
}

void RTC_Alarm_IRQHandler(void)
{
  HAL_RTC_AlarmIRQHandler(&hrtc);
  RTC_IT = 1;
}