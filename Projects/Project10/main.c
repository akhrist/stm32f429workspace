#include "stm32f429xx.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_exti.h"
#include "stm32f4xx_ll_rcc.h"

#define FLASH_KEY1 ((uint32_t)0x45670123)
#define FLASH_KEY2 ((uint32_t)0xCDEF89AB)

#define SectorNumber 3
#define SectorStart (unsigned long int*)0x0800C000
// Sector 3:  0x0800 C000 - 0x0800 FFFF, 16 Kbyte - 0x3FFF numbers

int ReadData();
void WriteData(int clicks);
void EraseSector(unsigned long sectorNumber);

void EXTI0_IRQHandler(void)
{
  LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_0);
  int clicks;
  clicks = ReadData();
  WriteData(clicks+1);
}

int ReadData()
{
  int i = 0;
  while( *(SectorStart + i) == 0 && i<0x3FFF/4)
  {
    i++;
  }
  return i-1;
}

void WriteData(int clicks)
{
  while((FLASH->SR)&FLASH_SR_BSY);
  FLASH->CR |= FLASH_CR_PG; //FLASH programming enable
  FLASH->CR &= ~FLASH_CR_PSIZE_0; //32-bit parallelism
  FLASH->CR |= FLASH_CR_PSIZE_1;  //32-bit parallelism
  while((FLASH->SR)&FLASH_SR_BSY);
  
  if(clicks >= 0x3FFF/4) EraseSector(SectorNumber);
  *(SectorStart+clicks) = 0;
  
  while((FLASH->SR)&FLASH_SR_BSY);
  FLASH->CR &= ~FLASH_CR_PG; //FLASH programming disable
}

void EraseSector(unsigned long sectorNumber)
{
  while((FLASH->SR)&FLASH_SR_BSY);
  FLASH->CR |= FLASH_CR_SER; //Sector erase enable
  FLASH->CR &= ~FLASH_CR_SNB; //Set sector for erasing
  FLASH->CR |= (FLASH_CR_SNB&(sectorNumber<<3));
  FLASH->CR |= FLASH_CR_STRT; //Start erase operation
  while((FLASH->SR)&FLASH_SR_BSY);
  FLASH->CR &= ~FLASH_CR_SER; //Sector erase disable
} 

int main()
{
  //button & light setup
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
  LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_0, LL_GPIO_MODE_INPUT);
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOG);
  LL_GPIO_SetPinMode(GPIOG, LL_GPIO_PIN_14, LL_GPIO_MODE_OUTPUT);
  
  //FLASH unlock
  FLASH->KEYR = FLASH_KEY1;
  FLASH->KEYR = FLASH_KEY2; 

  // Interrupts
  LL_EXTI_EnableRisingTrig_0_31(LL_EXTI_LINE_0);
  LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_0);
  __NVIC_EnableIRQ(EXTI0_IRQn);
  
  int number_of_clicks = ReadData();
  for (int i = 0; i <= number_of_clicks; i++)
  {
    GPIOG->ODR ^= GPIO_ODR_OD14;
    int j = 0;
    while ( j < 800000) j++;
    GPIOG->ODR ^= GPIO_ODR_OD14;
    j = 0;
    while ( j < 800000) j++;
  }
 
  while(1);
}
